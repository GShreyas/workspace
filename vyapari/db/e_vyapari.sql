-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 06, 2020 at 10:22 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_vyapari`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
CREATE TABLE IF NOT EXISTS `bill` (
  `bid` int(50) NOT NULL AUTO_INCREMENT,
  `oid` int(50) NOT NULL,
  `cid` int(50) NOT NULL,
  `amount` int(50) NOT NULL,
  `address` int(255) NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `oid` (`oid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cartid` int(50) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `iid` int(11) NOT NULL,
  `iname` varchar(50) NOT NULL,
  `quantity` int(50) NOT NULL,
  `rate` int(11) NOT NULL,
  `grndttl` int(50) NOT NULL,
  `picture` text NOT NULL,
  PRIMARY KEY (`cartid`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cartid`, `cid`, `iid`, `iname`, `quantity`, `rate`, `grndttl`, `picture`) VALUES
(31, 1, 36, 'wheat', 0, 12, 0, 'wheatflour.jpg'),
(32, 1, 34, 'rice', 0, 42, 0, 'rice.jpg'),
(33, 2, 34, 'rice', 0, 42, 0, 'rice.jpg'),
(34, 2, 36, 'wheat', 0, 12, 0, 'wheatflour.jpg'),
(35, 1, 34, 'rice', 0, 42, 0, 'rice.jpg'),
(36, 1, 34, 'rice', 0, 42, 0, 'rice.jpg'),
(37, 1, 34, 'rice', 0, 42, 0, 'rice.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `catid` int(50) NOT NULL AUTO_INCREMENT,
  `catname` varchar(50) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `cid` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` bigint(50) NOT NULL,
  `dob` date NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cid`, `username`, `name`, `password`, `address`, `phone`, `dob`) VALUES
(1, 'shreyas', 'shreyas', 'qwerty', 'vandige', 1234567890, '1997-06-01'),
(2, 'pavan', 'pavan palankar', 'qwerty', 'vandige', 8867764809, '1999-05-14'),
(3, 'Rocky', 'Rocky', 'qwerty', 'qwwerg', 8867764809, '2019-04-12'),
(4, 'bullu', 'bullu', 'qwerty', '', 0, '1996-01-05'),
(5, 'Ramu', 'Ramu', 'qwerty', '', 0, '1998-02-03'),
(6, 'vinutan', 'vinutan', 'qwerty', 'tenkankaeri', 1234567890, '1998-02-02');

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

DROP TABLE IF EXISTS `delivery`;
CREATE TABLE IF NOT EXISTS `delivery` (
  `did` int(50) NOT NULL AUTO_INCREMENT,
  `dname` varchar(50) NOT NULL,
  `phone` int(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `picture` longtext NOT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `iid` int(50) NOT NULL AUTO_INCREMENT,
  `iname` varchar(50) NOT NULL,
  `catid` int(50) NOT NULL,
  `sid` int(50) NOT NULL,
  `rate` double NOT NULL,
  `description` varchar(255) NOT NULL,
  `picture` text NOT NULL,
  `catname` varchar(50) NOT NULL,
  PRIMARY KEY (`iid`),
  KEY `catid` (`catid`),
  KEY `sid` (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`iid`, `iname`, `catid`, `sid`, `rate`, `description`, `picture`, `catname`) VALUES
(34, 'rice', 0, 0, 42, 'Good quality rice.', 'rice.jpg', 'fg'),
(35, 'Wheat Flour', 0, 0, 52, 'Good quality', 'wheatflour.jpg', 'fg'),
(36, 'wheat', 0, 0, 12, 'good', 'wheatflour.jpg', 'fg');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `oid` int(50) NOT NULL AUTO_INCREMENT,
  `iid` int(50) NOT NULL,
  `cid` int(50) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`oid`),
  KEY `iid` (`iid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
CREATE TABLE IF NOT EXISTS `shop` (
  `sid` int(50) NOT NULL AUTO_INCREMENT,
  `sname` varchar(50) NOT NULL,
  `ownname` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `sphone` int(50) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`sid`, `sname`, `ownname`, `address`, `sphone`) VALUES
(1, 'tamburi', 'tamburi', 'ankola', 123456789),
(2, 'vaishya traders', 'harish', 'vandige', 1234567890);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`iid`) REFERENCES `item` (`iid`),
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`cid`) REFERENCES `customer` (`cid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
