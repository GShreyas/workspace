<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
<link rel="stylesheet" type="text/css" href="css/experiment.css">
</head>
<body>
<div class="header">
	<img class="logo" src="..\vyapari\Images\Logo.png">
  <p>E-Vyapari<p>
   </div>
 <div class="navbar">
  <a href="#">Home</a> 
  <a href="#">Offers</a>
  <a href="logout.php">Log out</a>
  	<div class="dropdown">
		<a class="dropdown-btn">Categories</a>
			<div class="dropdown-content">
				<div class="submenu">
					<a class="submenu-link" href="#">Food Grains & Pulses</a>
						<div class="submenu-content">
							<a href="#">Rice</a>
							<a href="#">Wheat</a>
							<a href="#">Barley</a>
							<a href="#">Maize</a>
						</div>	
				</div>
				<div class="submenu">
					<a class="submenu-link" href="#">Fruits and Vegetables</a>
						<div class="submenu-content">
							<a href="#">Apple</a>
							<a href="#">Banana</a>
							<a href="#">Onion</a>
							<a href="#">Brinjal</a>
						</div>	
				</div>
				<div class="submenu">
					<a class="submenu-link" href="#">Cleaning & Household</a>
						<div class="submenu-content">
							<a href="#">Vim</a>
							<a href="#">Dettol</a>
							<a href="#">Rin</a>
							<a href="#">Lux</a>
						</div>	
				</div>
			</div>
	</div>		
  
  <a href="#" class="cart">Cart</a>
  <button class="right" onclick="openForm()">SignIn</button>
  <div class="form-popup" id="myForm">
 <div class="container">
			<h1 class="header">Login</h1>
		<form action="login_dup.php" method="post">	
			<div class="input">
				<label>Username</label>
				<input type="text" name="username" placeholder="Username" required>
			</div>
			<div class="input">
				<label>Password</label>
				<input type="password" name="password" placeholder="Password" required>
			</div>
	
			<input type="submit" name="submit" value="submit">
			<p><a class="register" href="..\vyapari\sign.php">Click here to register</a></p>
			<div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
			<button type="button" class="btn cancel" onclick="closeForm()">Close</button>
		</form>
</div>

<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
</script>
  </div>
<!--<div class="row">-->
   <div class="main">
	<div class="image">
			<div class="picture1">
				<img src="..\vyapari\Images\brinjal.JPG">
			</div>
			<div class="description">
					<p>Brinjal</p>
			</div>
			
				
	</div>
	<div class="image">
			<div class="picture1">
				<img src="..\vyapari\Images\onion.jpg">
			</div>
			<div class="description">
					<p>Onion</p>
			</div>
				
	</div>
	<div class="image">
			<div class="picture1">
				<img src="..\vyapari\Images\potato.jpg">
			</div>
			<div class="description">
					<p>Potato</p>
			</div>
				
	</div>
			
</div>
	
<!--</div>-->
<!--<div class="footer">
  <a href="#">About us</a>
  <a href="#">Contact us</a>
  -->
</div>
</body>
</html>