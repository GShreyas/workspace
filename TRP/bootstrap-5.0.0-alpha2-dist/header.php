<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="js/jquery-3.5.1.min.js"></script>
    <title>TRP</title>
</head>

<body> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                    <nav class="navbar navbar-expand-lg navbar-dark" style="background-color:#4872aa">
                        <a class="navbar-brand" href="#"><img class="img-responsive" src="images/home_01copy.png"
                                style="width:250px"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon" style="color:#fff"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="index.php" id="hom" style="color:#fff"
                                        data-toggle="collapse" data-target=".navbar-collapse.show">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" id="srv" style="color:#fff" data-toggle="collapse"
                                        data-target=".navbar-collapse.show">Services</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" id="job" style="color:#fff" data-toggle="collapse"
                                        data-target=".navbar-collapse.show">Jobs featured</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" id="adv" style="color:#fff" data-toggle="collapse"
                                        data-target=".navbar-collapse.show">Advantages</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" id="con" style="color:#fff" data-toggle="collapse"
                                        data-target=".navbar-collapse.show">Contact us</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>