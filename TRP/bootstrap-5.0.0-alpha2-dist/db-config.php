<?php

class DBConfig
{
    //$host = "localhost";
    //$db_name = "trp";
    //$username = "root";
    //$password = "qwerty";
    protected static $conn = null;
    protected static function dbConnect()
    {
        if (self::$conn == null) {
            self::connect();
        }
        try {
        } catch (PDOException $exception) {
            self::connect();
        }
        return self::$conn;
    }

    protected static function connect($host = "localhost", $db_name = "trp", $username = "root", $password = "")
    {
        try { //database connection
            self::$conn = new PDO("mysql:host={$host};dbname={$db_name};charset=UTF8",
                $username, $password);
            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) { //to handle connection error
            echo "Connection error: " . $exception->getMessage();
            error_log($exception->getMessage());
        }
    }
}
