<!-- Constant -->
<div class=container-fluid>
    <div class="row ">
        <div class="col-md-3 d-none d-md-block">
            <div class="card" style="border-top:none;border-bottom:none;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                <div class="card-header">
                    <img src="images/clients.jpg" class="img-responsive" style="height:250px;width:100%;">
                </div>
                <div class="card-body">
                    <h4 class="text-center">Featured Jobs</h4>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item">Branch Manager</li>
                        <li class="list-group-item">Policy Manager</li>
                        <li class="list-group-item">Product Head - Mortgage</li>
                        <li class="list-group-item">Area Sales Manager</li>
                        <li class="list-group-item">Senior Credit Analyst</li>
                        <li class="list-group-item"><a href="#" id="more">Click here to know more</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-------->
        <div class="col-md">
            <!-- Landing -->
            <div class="container-fluid">
            <div class="row" id="land">
                <div class="col-md-12">
                    <p style="padding-top: 10px;">TRP Consultants Pvt. Ltd. better known as the Right People, is an
                        established name in executive search and recruitment.
                        We started in 1988, at a time when the concept of placement services was still in its nascent
                        stage in India.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3" id="land1">
                    <div class="card">
                        <img src="images/home_page1.jpg" class="img-responsive" style="height:200px; width:100%;">
                    </div>
                </div>
                <div class="col-md-9 " id="land2">
                    <p>Our thoroughly professional approach gained the confidence of corporate India,
                        specifically of the elite players in the banking, financial services, insurance and
                        manufacturing sectors.
                        We understand clients requirements and proactively search and identify suitable and interested
                        candidates
                        within the stipulated turnaround time. Our long association with clients is testimony to our
                        high quality service.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="land3">
                    <p>In recognition of our excellent service provided to ICICI Bank Ltd over the last ten years,
                        the bank Conferred on us the " Top Recruiter of the Year " award in 2006.
                    </p>
                    <img src="images/laptop_2.jpg" class="img-fluid mx-auto d-block">
                </div>
            </div>


            <!-- Services -->
            <div class="row" id="services" style="display:none;">
                <div class="col-md-12">
                    <img src="images/stairs1.jpg" class="img-fluid" style="height:640px ; width:100%;">
                </div>
            </div>
            <!-- Advantages -->
            <div class="row" id="advant" style="display:none;">
                <div class="col-md-12">
                    <h2 class="text-center">Advantages</h2>
                    <div class="card text-center mt-3">
                        <div class="card-body">
                            <h4 class="card-title">PRUDENT APPROACH</h4>
                            <p class="card-text">Thorough understanding of the clients requirements.</p>
                        </div>
                    </div>
                    <div class="card text-center mt-3">
                        <div class="card-body">
                            <h4 class="card-title">PROACTIVE APPROACH</h4>
                            <p class="card-text">Proactive search and identification of suitable and interested
                                candidates.</p>
                        </div>
                    </div>
                    <div class="card text-center mt-4">
                        <div class="card-body">
                            <h4 class="card-title">HIGH TRACK RECORD</h4>
                            <p class="card-text">Completion of assignments in stipulated turnaround times.</p>
                        </div>
                    </div>
                    <div class="card text-center mt-4">
                        <div class="card-body">
                            <h4 class="card-title">PROCESS ORIENTATION</h4>
                            <p class="card-text">Completion of assignments in stipulated turnaround times.</p>
                        </div>
                    </div>
                    <div class="card text-center mt-4 mb-1">
                        <div class="card-body">
                            <h4 class="card-title">REFERENCE CHECKING</h4>
                        </div>
                    </div>


                </div>
            </div>
            <!-- Jobs featured-->
            <div class="container" id="jobs" style="display:none">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center">Current Featured Jobs</h2>
                        <div class="form-group text-center mt-1">
                            <select class="form-control mx-auto" id="sel1" style="width:250px;">
                                <option>Select an Industry:</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed">
                                <thead id="tblhd" style="display:none">
                                    <tr>
                                        <td>Job title</td>
                                        <td>Client</td>
                                        <td>State</td>
                                        <td>Location</td>
                                        <td>Apply</td>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Contact Us -->
            <div class="row " id="contact" style="display:none">
                <div class="col-md-12">
                    <h2 class="text-center">Contact Us</h2>
                    <div class="row mt-5">
                        <div class="col-md">
                            <div class="card" style="border:none">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15075.08391817493!2d72.851067!3d19.1614997!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x88c50aec9dfad8de!2sTRP%20Consultants%20Pvt.%20Ltd!5e0!3m2!1sen!2sin!4v1604499366245!5m2!1sen!2sin"
                                    width="450" height="450" style="border:0;" allowfullscreen="" aria-hidden="false"
                                    tabindex="0"></iframe>
                            </div>
                        </div>
                        <div class="col-md text-center">
                            <div class="card h-100">
                                <address class="my-auto">
                                    211, Udyog Bhavan, Sonawala Road,
                                    Goregaon East .<br>
                                    Mumbai - 400 063. <br>
                                    <abbr title="Phone">P:</abbr> +91 022 6002 1101
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <div> 
            <script>
            $(document).ready(function() {
                //Home

                //Services
                $('#srv').click(function() {
                    $('#land').hide();
                    $('#land1').hide();
                    $('#land2').hide();
                    $('#land3').hide();
                    $('#land4').hide();
                    $('#land5').hide();
                    $('#contact').hide();
                    $('#advant').hide();
                    $('#jobs').hide();
                    $('#services').show();
                });
                //Advantages
                $('#adv').click(function() {
                    $('#land').hide();
                    $('#land1').hide();
                    $('#land2').hide();
                    $('#land3').hide();
                    $('#land4').hide();
                    $('#land5').hide();
                    $('#services').hide();
                    $('#jobs').hide();
                    $('#contact').hide();
                    $('#advant').show();
                });
                //Jobs featured

                $('#job').click(function() {
                    $('#land').hide();
                    $('#land1').hide();
                    $('#land2').hide();
                    $('#land3').hide();
                    $('#land4').hide();
                    $('#land5').hide();
                    $('#services').hide();
                    $('#advant').hide();
                    $('#contact').hide();
                    $('#jobs').show();
                });
                //Contact
                $('#con').click(function() {
                    $('#land').hide();
                    $('#land1').hide();
                    $('#land2').hide();
                    $('#land3').hide();
                    $('#land4').hide();
                    $('#land5').hide();
                    $('#services').hide();
                    $('#advant').hide();
                    $('#jobs').hide();
                    $('#contact').show();
                });

                $('#more').click(function() {
                    $('#land').hide();
                    $('#land1').hide();
                    $('#land2').hide();
                    $('#land3').hide();
                    $('#land4').hide();
                    $('#land5').hide();
                    $('#services').hide();
                    $('#advant').hide();
                    $('#contact').hide();
                    $('#jobs').show();
                });

                $('#sel1').click(function() {
                    $('#tblhd').show();

                });
            });
            </script>
            <script>
            $(document).ready(function() {
               //alert("xyz");
                $.ajax({
                    type: "GET",
                    url: "http://localhost/myprojects/trp/bootstrap-5.0.0-alpha2-dist/controller.php?doaction=getInd",
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function(response) {
                       
                       alert(response);
                       // console.log("Response------------>" + response);
                        var select = '';
                        $.each(response, function(i, ind) {
                            select += "<option class='mx auto' id='opt' value='" + ind
                                .industryID + "' >" + ind.industryName + "</option>";
                        });
                       // console.log("Select---------------------->" + select);
                        $('#sel1').append(select);

                    },

                });
            });
            </script>
            <script>
            function load(sval) {
                $.ajax({
                    type: "GET",
                    url: "http://localhost/myprojects/trp/bootstrap-5.0.0-alpha2-dist/controller.php?doaction=getReq&oid=" +
                        sval,
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function(response) {
                        //console.log(response);
                        var table = '';
                        $.each(response, function(i, req) {
                            table += '<tr><td><a target="_blank" href="view-job.php?jid=' + req
                                .jobID + '">' + req.position +
                                '</a></td><td>' + req.clientName + '</td><td>' + req.state +
                                '</td><td>' + req.location +
                                '</td><td><a target="_parent" href="apply.php?jobid=' + req.jobID +
                                '&jobnm=' + req.position +
                                '">Apply now</a></td></tr>';
                        });
                        //console.log(table);
                        $('#myTable').html(table);
                    },
                    error: function(e) {
                        //console.log(response);
                    }
                });
            };
            $(document).ready(function() {
                $('#sel1').on('change', function() {
                    var selVal = $('#sel1').val();
                    //alert(x);
                    load(selVal);
                });
            });
            </script>
        </div>
    </div>
</div>