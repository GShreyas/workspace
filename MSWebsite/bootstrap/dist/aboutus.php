<?php
include 'header.php';
include 'navbar.php';
?>
<!-------------------------------------------------------Hero--------------------------------------------------->
<section>
	<div class="container-fluid aboutus-cover">
		<div class="row">
		 <div class="col-sm-12">
			<h1 class="text-white fw-bold mt-6">
				About Us
			</h1>
		 </div>
		</div>
	</div>	
</section>
<!--------------------------------------------------Who we are--------------------------------------------------->
<section>
	<div class="container-fluid">
		<div class="row h-100 pt-5">
			<div class="col-md-4 col-sm-12" id="who_we-head">
				<h2 class="fw-bold">Who We Are</h2>
			</div>
			<div class="col-md-1 flexing pe-0">
				<div class="vl"></div>
			</div>
			<div class="col-md-7 col-sm-12 flexing ps-0">
				<p class="pt-4 who-we_p who-we_mobile-m">Maruthi Solar Systems was started in the year 1998. It is based in Bangalore, India and is solely dedicated to designing , manufacturing and supplying in the line of electronic products related to power electronics, renewable energy and other related products.</br></br>
				We are proud to claim that we are one of the major contract manufacturers of Electronics products for many OEM’s like Bharat Electronics, BHEL, Tata Power Solar, Reliance Solar and many other private system integrators in India.</br></br>
				Our company is licensed by MNRE to support project sponsored by Government of India. We have a R&D based manufacturing set up in Bangalore India under the company banner Maruthi Solar Systems with manufacturing area of 6000 sq ft.
				</p>
			</div>
		</div>
	</div>
</section>
<!--------------------------------------------------ISO--------------------------------------------------->
<section>
	<div class="container-fluid">
		<div class="row h-100 pt-5">
			<div class="col-md-4 d-none d-sm-block" id="iso_head">
				<h2 class="fw-bold text-center pt-4">ISO 9001-2015</br> certified and</br> NSIC/MSME</br> registered</br> company.</h2>
			</div>
			<div class="col-md-4 col-sm-12 d-block d-sm-none">
				<h2 class="fw-bold fs-2 text-center">ISO 9001-2015 certified and NSIC/MSME registered company.</h2>
			</div>
			<div class="col-md-1 flexing pe-0">
				<div class="vl"></div>
			</div>
			<div class="col-md-7 col-sm-12 flexing ps-0">
				<p class="pt-5 who-we_mobile-m">Our main strength lies in understanding the customer requirements, meticulous planning, dedication and hard work of our team. We also specialize in design, manufacturing, supply and installation of Solar Lighting systems, Off Grid and On Grid Solar Systems.</br></br>
 
               With 22 years of hard efforts, we are becoming more and more internationalized through our passionate employees and strong cooperative R&D capabilities; we can react quickly to our customer’s needs and provide comprehensive, customized & complete products.
				</p>
			</div>
		</div>
	</div>
</section>
<!--------------------------------------------------Strengths--------------------------------------------------->
<section>
	<div class="container-fluid">
		<div class="row pt-5">
			<div class="col-md-12 col-sm-12">
				<h1 class="text-center fw-bold pb-5">Our Strengths</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12 flexing">
				<div class="card text-center border-0 card-str" >
					<img src="images/eng2.png" class="mx-auto">
					<span class="card-title mt-2 fw-bold">Engineering</span>
					<p class="card-text">Design the system according to the requirement of the customer and the site conditions.</p>
				</div>
			</div>
			<div class="col-md-4  col-sm-12 flexing">
				<div class="card text-center border-0 card-str">
					<img src="images/research2.png" class="mx-auto">
					<span class="card-title mt-2 fw-bold">R&D</span>
					<p class="card-text">Our in-house R&D caters to</br> customer requirements with speedy solutions.</p>
				</div>
			</div>
			<div class="col-md-4  col-sm-12 flexing">
				<div class="card text-center border-0 card-str" >
					<img src="images/mfg1.png" class="mx-auto">
					<span class="card-title mt-2 fw-bold">Manufacturing</span>
					<p class="card-text">Our manufacturing facility is</br> ISO 9001:2015</br> certified.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-card_ic" >
				<div class="card text-center border-0 card-str" id="card-ic">
					<img src="images/ic1.png" class=" mx-auto">
					<span class="card-title mt-2 fw-bold">I & C</span>
					<p class="card-text">Highly experienced and dedicated team with engineers for faster and successful execution.</p>
				</div>
			</div>
			<div class="col-md-6 col-card_ass" >
				<div class="card text-center border-0 card-str" id="card-ass">
					<img src="images/S&S2.png" class="mx-auto">
					<span class="card-title mt-2 fw-bold">After Sales Services</span>
					<p class="card-text">Guaranteed after sales support & services for businesses situated in various locations.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!--------------------------------------------------Certificates-------------------------------------------------->
<section>
	<div class="container-fluid mb-5">
		<!--ISO Modal-->
		<div class="modal fade" id="isoModal" tabindex="-1" aria-labelledby="isoModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="isoModalLabel">ISO 9001:2015</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<img src="images/iso.jpg" class="img-fluid">
			  </div>
			</div>
		  </div>
		</div>
		<!--MSME Modal-->
		<div class="modal fade" id="msmeModal" tabindex="-1" aria-labelledby="msmeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-fullscreen">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="msmeModalLabel">ISO 9001:2015</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<img src="images/msme.jpg" class="img-fluid">
			  </div>
			</div>
		  </div>
		</div>
		<!--NSIC Modal-->
		<div class="modal fade" id="nsicModal" tabindex="-1" aria-labelledby="nsicModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="nsicModalLabel">ISO 9001:2015</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<img src="images/nsic1.jpg" class="img-fluid">
			  </div>
			</div>
		  </div>
		</div>
		<div class="row py-4">
			<div class="col-md-12 col-sm-12">
				<h1 class="text-center fw-bold">Certificates</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#isoModal" class="mx-auto">
						<img src="images/iso.jpg" class="cert-img" >
					</a>	
					<div class="card-title text-center fw-bold mt-3">ISO 9001:2015</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#msmeModal" class="mx-auto">
						<img src="images/msme.jpg" class="img-fluid cert-img" >
					</a>
					<div class="card-title text-center fw-bold mt-3">MSME</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#nsicModal" class="mx-auto">
						<img src="images/nsic1.jpg" class="cert-img" >
					</a>
					<div class="card-title text-center fw-bold mt-3">NSIC</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include 'footerbar.php';
include 'footer.php';
?>