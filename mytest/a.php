<?php

echo "Hello!" . "<br>";

class Fruit
{
    private $name;
    private $color;

    public function __construct($name, $color)
    {
        echo "Fruit constructor" . "<br>";
        $this->name = $name;
        $this->color = $color;
    }
    public function intro()
    {
        echo "Fruit intro" . "<br>";
        echo "The fruit is {$this->name} and the color is {$this->color}.<br>";
    }
}

// Strawberry is inherited from Fruit
class Strawberry extends Fruit
{
    private $count;
    public function __construct($name, $color)
    { 
        echo "Strawberry Constructor<br>";
        parent::__construct($name,$color);
        $this->count = 10;
    }

    public function message()
    {
        echo "Am I bbbbnn fruit or a berry? <br>";
    }
}
$strawberry = new Strawberry("Strawberry", "red");
$strawberry->message();
$strawberry->intro();
