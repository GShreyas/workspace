<?php 
if (isset($_SERVER['HTTP_ORIGIN'])) {
  
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
}else{
    header("Access-control-Allow-Origin: *"); 
}
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Max-Age: 86400');    

  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT");         
    
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once 'classCategory.php';
require_once 'classProduct.php';
require_once 'db_config.php';
    $action = $_GET['doaction'];
   if($action=="getcategories")
    {
      $obj= new Category($conn);
      $data= $obj->getCategories(); 
    }
    else if($action=="getproducts")
    {
      $obj= new Products($conn);
      $data = $obj->getProducts();
    }
    else if($action="getprcat")
    {
      $catgid  = $_GET['catId'];
    $obj= new Products($conn);
    $data= $obj->getPrcat($catgid);
    }
    echo json_encode($data);
?>