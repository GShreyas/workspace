
<?php require('include/header.php') ?>
	
	<div class="container-fluid">
		<img class="d-block w-100" src="imgs/solar_panel1.jpg" alt="solar panel">
		<div>
			<!--<h4>Maruthi Solar Systems</h4>
			<p class="font-weight-normal">Our strength lies in System design, Product development, Sourcing, Manufacturing, Installation & Commissioning of Solar Photovoltaic Systems.</p>-->
		</div>
	</div>
	<!--Chnagesss-->
	<!--Jumbotron-->
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="jumbotron" style="background-color:#007bff; color:#fff;">
						<h3 class="ml12">FEEL THE CHANGE!</h3>
						<h6 class="text-center">WE DELIVER OFF-GRID AND ON-GRID SOLUTIONS, CUSTOMIZED AS PER YOUR NEEDS.</h6>
					</div>
				</div>
			</div>
		</div>
	<!--/.Jumbotron-->
	
	<!--Cards-->
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<a href="contact.php">	<div class="card" id="thisCard">
						<img class="card img-top" src="imgs/installer.jpg" alt="Card image">
						<div class="card-body">
							<h5 class="card-title text-center">INSTALLERS/INTEGRATORS</h5>
						</div>
					</div>
				</a>		
				</div>
				<div class="col-md-6">
					<div class="card" id="thisCard">
						<img class="card img-top" src="imgs/oem.jpg" alt="Card image">
						<div class="card-body">
							<h5 class="card-title text-center">OEMS</h5>
						</div>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="card" id="thisCard">
						<img class="card img-top" src="imgs/ig.jpg" alt="Card image">
						<div class="card-body">
							<h5 class="card-title text-center">GOVERNMENT</h5>
						</div>
					</div>	
				</div>
				<div class="col-md-6">
					<div class="card" id="thisCard">
						<img class="card img-top" src="imgs/dealers.jpg" alt="Card image">
						<div class="card-body">
							<h5 class="card-title text-center">DISTRIBUTORS/DEALERS</h5>
						</div>
					</div>	
				</div>
			</div>
			
		</div>
		
	<!--/.Cards-->
	
	<!--Jumbotron-->
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="jumbotron" style="background-color:#007bff; color:#fff;">
						<h3 class="text-center">OUR PRODUCTS</h3>
						<h6 class="text-center">WE OFFER VARIOUS POWER SOLUTIONS FOR YOUR NEEDS.</h6>
					</div>
				</div>
			</div>
		</div>
	<!--/.Jumbotron-->

	<!--Card Groups-->
	
<div class="container-fluid">	
	<div class="row">
		<div class="col-md-12">
			<div class="card-group">
				<div class="card" style="border-top-style: none; border-bottom-style: none;">
					<div class="card-body text-center">
						<img src="imgs/13.jpg" class="img-fluidic">
						<h5>Charge Controller</h5>
						<p class="card-text">Maruthi's Solar Charge Controller product lines are the best option for compact, stand alone solar systems.</p>
					</div>
				</div>
				<div class="card" style="border-top-style: none; border-bottom-style: none;">
						<img src="imgs/10.jpg" class="img-fluidic">
						<div class="card-body text-center">
							<h5>Street Lighting System</h5>
							<p class="card-text">MARUTHI's solar street lighting systems are designed to operate as one integrated system.</p>
						</div>
				</div>
				<div class="card" style="border-top-style: none; border-bottom-style: none;">
						<img src="imgs/03.jpg" class="img-fluidic">
						<div class="card-body text-center">
							<h5>Solar Inverter</h5>
							<p class="card-text">Maruthi's Solar Inverter can power domestic loads like Personal Computers, Laptop, Incandescent bulbs, CFL lamps, Fluorescent lamps, Halogen lamps, Fans, small mixers, blenders, food processors, TV, music system, DVD player etc</p>
						</div>
				</div>
				<div class="card" style="border-top-style: none; border-bottom-style: none;">
						<img src="imgs/28.jpg" class="img-fluidic">
						<div class="card-body text-center">
							<p class="card-text">Some text inside the fourth card</p>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style="margin-top:40px;">
					<a href="products.php" class="btn btn-primary btn-block">VIEW PRODUCTS</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron" style="background-color:#007bff; color:#fff;margin-top:40px;">
				<h3 class="text-center">CERTIFICATES</h3>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<img id="myImg" src="imgs/iso.jpg" alt="ISO" style="width:100%">
		</div>
		<div class="col-md-4">
		<img id="myImg" src="imgs/msme.jpg" alt="MSME" style="width:100%">
			
		</div>
		<div class="col-md-4">
		<img id="myImg" src="imgs/nsic1.jpg" alt="NSIC" style="width:100%">
			

		</div>
	</div>
</div>

<!--/.Card Groups-->

<?php require 'include/footer.php' ?>