<?php
//session_start();
require_once '../db_config.php';
require_once '../classContact.php';
require_once 'classChecklogin.php';
  
$val = new checkLogin($conn);
$check = $val->loginCheck();   

$obj= new Contact($conn);
$data= $obj->viewInfo();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="wvalueth=device-wvalueth, initial-scale=1">
	<title>Admin Panel</title>
	<meta name="description" content="Admin Content">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/base.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--<script src="js/bootstrap.js"></script>
    <script src="js/jquery-3.4.1.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
	</script> <![endif]-->
</head>
<body>
<div class="container-fluid">
<h2><a href="adminPanel.php">Admin Panel</a></h2>
  <div class="row">
  <div class="col-md-3">
    <ul class="nav flex-column">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" value="navbardrop" data-toggle="dropdown" href="#">Products</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="addProducts.php">Add Products</a>
            <!--<a class="dropdown-item" href="#">Link 2</a>
            <a class="dropdown-item" href="#">Link 3</a>-->
        </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" value="navbardrop" data-toggle="dropdown" href="#">Categories</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="addCategories.php">Add Categories</a>
            <a class="dropdown-item" href="view-category.php">View Categories</a>
            <a class="dropdown-item" href="#">Link 3</a>
        </div>
    </li>
    <li class="nav-item">
    <a class="nav-link dropdown-toggle" value="navbardrop" data-toggle="dropdown" href="#">Form</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="view-form.php">View form</a>
        </div>
    </li>
    <li class="nav-item"><a href="logout.php">Logout</a></li>
  </ul>
  </div>
  <div class="col-md">
  <div class="table-responsive">
        <table class="table condensed">
          <thead>
            <tr>
              <td>Name</td>
              <td>Designation</td>
              <td>Organisation</td>
              <td>Email</td>
              <td>City</td>
              <td>Phone No.</td>
              <td>Requirements</td>
              <td>Operations</td>
            </tr>
          </thead>
          <tbody>
            <?php
              foreach($data as $key => $value){

            ?>
            <tr>
              <td><?php echo $value['name']?></td>
              <td><?php echo $value['dsgn']?></td>
              <td><?php echo $value['org']?></td>
              <td><?php echo $value['email']?></td>
              <td><?php echo $value['city']?></td>
              <td><?php echo $value['phno']?></td>
              <td><?php echo $value['req']?></td>
              <td><a href="del-form.php?fid=<?php echo $value['fid'] ?>" >Delete</a></td>
            </tr>
              <?php } ?>
          </tbody>
        </table>
  </div>
  
  </div>
</div>

</body>