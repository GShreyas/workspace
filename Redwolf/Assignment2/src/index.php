<?php include "validate.php" ; ?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
    <div class="header">
        <img src="redwolf.png" alt="Logo"/>
        <h1><b>SUBMIT YOUR DESIGN</b></h1>
    </div>
    <div class="container" id="primary">
        <form method="post">
            <div class="input">
                <label for="fname">Name:</label>
                    <input type="text" id="fname" name="fname" value="<?php echo $name ?? ''; ?>" required><span
                    class="error"><?php echo $name_error; ?></span><br>
            </div>
            <div class="input">
                <label for="mail">E-mail:</label>
                <input type="text" id="mail" name="mail" value="<?php echo $mail ?? ''; ?>" required><span
                    class="error"><?php echo $email_error; ?></span><br>
            </div>
            <div class="input">
                <label for="desgtitle">Design Title:</label>
                <input type="text" id="desgtitle" name="desgtitle" value="<?php echo $desgttl ?? ''; ?>"
                  required ><span class="error"><?php echo $design_error; ?></span><br>
            </div>
            <div class="input">
                <label for="radio button">Gender:</label>
                    <input type="radio" id="male" name="gender" value="Male"
                    <?php echo $gender && $gender=="Male" ? 'checked' : '';?>>
                <label for="male">Male</label>
                <input type="radio" id="female" name="gender" value="Female"
                    <?php echo $gender && $gender=="Female" ? 'checked' : '';?>>
                <label for="female">Female</label><span class="error"
                    style="margin-left:97px"><?php echo $gender_error; ?></span><br>
            </div>
            <div id="maleColor" class="input">
                <label for="color">Colour:</label>
                <select id="color" name="maleColor">
                    <option value="">Select Tee Colour</option>
                    <option <?php echo $color=='White'?'selected':''; ?>>White</option>
                    <option <?php echo $color=='Black'?'selected':''; ?>>Black</option>
                    <option <?php echo $color=='Red'?'selected':''; ?>>Red</option>
                    <option <?php echo $color=='Grey'?'selected':''; ?>>Grey</option>
                </select>
                <span class="error"><?php echo $color_error; ?></span><br>
            </div>
            <div id="femaleColor" class="input" style="display:none">
                <label for="color">Colour:</label>
                <select id="color2" name="femaleColor">
                    <option value="">Select Tee Colour</option>
                    <option <?php echo $color=='White'?'selected':''; ?>>White</option>
                    <option <?php echo $color=='Black'?'selected':''; ?>>Black</option>
                </select>
                <span class="error"><?php echo $color_error; ?></span><br>
            </div>
            <div class="input">
                <label for="camp">Start Campaign immediately?</label>
                <input type="hidden" name="camp" value="0">
                <input type="checkbox" id="camp" name="camp" value="1"
                    <?php echo $camp=='1' || $camp=="" ?'checked':''; ?>><br>
            </div>
            <div class="input" id="hideThis">
                <label for="startdate">Campaign start date:</label>
                <input type="text" id="datepick" name="startdate" value="<?php echo $date ?? ''; ?>"><span
                    class="error"><?php echo $date_error; ?><span><br>
            </div>

            <button type="submit" id="subbtn" name="submit"><b>SUBMIT</b></button>
            <div class="output"><?php echo $output; ?></div>
        </form>
    </div>
    <script type="text/javascript">
    //Datepicker for campaign date
    $(function() {
        $("#datepick").datepicker({
          dateFormat: "dd-M-yy" ,
          // minDate : 0 
        });
    });
    //Show or hide campaign date based on the checkbox
    $("#camp").change(function() {
        //console.log('#camp changed function');
        if ($("#camp").is(':checked'))
            $("#hideThis").hide(); // checked
        else
            $("#hideThis").show(); // unchecked

    });
    //Show or hide the colours based on selected gender
    $("input[type='radio'][name='gender']").change(function() {
        //console.log('val='+$(this).val());
        if ($(this).val() == "Male") {
            $('#femaleColor').hide();
            $('#maleColor').show();
        } else {
            $('#femaleColor').show();
            $('#maleColor').hide();

        }
    });

    //When document ready show/hide the camp/gender
    $(document).ready(function() {
        //console.log('Document ready event fired');
        //camp
        if ($("#camp").is(':checked'))
            $("#hideThis").hide(); // checked
        else
            $("#hideThis").show(); // unchecked

        //gender
        if ($('#male').is(':checked')) { //male checked
                $('#femaleColor').hide();
                $('#maleColor').show();
        } else  {                        //female checked
                $('#femaleColor').show();
                $('#maleColor').hide();

        }     
    });
    </script>
</body>
</html>